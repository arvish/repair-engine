package com.repairengine.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepairEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(RepairEngineApplication.class, args);
	}

}
